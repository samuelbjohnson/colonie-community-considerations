# Colonie Community Considerations

Letters, petitions, notes, etc. on improvements I would like to see made by the Town of Colonie, New York.

I'm using Git in the hopes that other members of the Colonie community will collaborate with me. Letters and petitions always carry more weight when they have more names behind them. I also don't claim to have all the answers: git provides a ready means for others to offer suggestions or improvements.
